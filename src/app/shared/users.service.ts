import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { user } from '../models/user';

@Injectable({
	providedIn: 'root'
})
export class UsersService {
	apiURL = 'http://localhost:5000/';

	constructor(private http: HttpClient) {}

	createUser(post: user) {
		console.log(post);
		return this.http.post<user>(`${this.apiURL}api/auth/register`, post);
	}

	loginUser(connect: user) {
		return this.http.post<user>(`${this.apiURL}api/auth/login`, connect);
	}

	getUser() {
		return this.http.get(`${this.apiURL}api/auth/me`);
	}
}
