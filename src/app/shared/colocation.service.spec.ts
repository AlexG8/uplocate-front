import { TestBed } from '@angular/core/testing';

import { ColocationService } from './colocation.service';

describe('ColocationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColocationService = TestBed.get(ColocationService);
    expect(service).toBeTruthy();
  });
});
