import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { user } from '../models/user';

@Injectable({
	providedIn: 'root'
})
export class ColocatairesService {
	apiURL = 'http://localhost:5000/';

	constructor(private http: HttpClient) {}

	getUsers(): Observable<user[]> {
		return this.http.get<user[]>(`${this.apiURL}api/user`);
	}
}
