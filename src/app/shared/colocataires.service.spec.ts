import { TestBed } from '@angular/core/testing';

import { ColocatairesService } from './colocataires.service';

describe('ColocatairesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ColocatairesService = TestBed.get(ColocatairesService);
    expect(service).toBeTruthy();
  });
});
