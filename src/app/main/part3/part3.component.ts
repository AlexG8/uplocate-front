import { Component, OnInit } from '@angular/core';
import { user } from '../../models/user';
import { Observable } from 'rxjs';
import { ColocatairesService } from '../../shared/colocataires.service';
import { environment } from '../../../environments/environment'

@Component({
	selector: 'app-part3',
	templateUrl: './part3.component.html',
	styleUrls: [ './part3.component.css' ]
})
export class Part3Component implements OnInit {
	getColocataire$: Observable<user[]>;
	imagePath = environment.imagePath;


	constructor(private colocSvc: ColocatairesService) {}

	ngOnInit() {
		this.getColocataire$ = this.colocSvc.getUsers();
	}
}
