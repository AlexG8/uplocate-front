import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackgroundSearchbarComponent } from './background-searchbar.component';

describe('BackgroundSearchbarComponent', () => {
  let component: BackgroundSearchbarComponent;
  let fixture: ComponentFixture<BackgroundSearchbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackgroundSearchbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
