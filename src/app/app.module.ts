import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// pour les requetes http vers l'api
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { JwtModule } from '@auth0/angular-jwt';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './routing/app-routing.module';
import { NavBarComponent } from './header/nav-bar/nav-bar.component';
import { BackgroundSearchbarComponent } from './header/background-searchbar/background-searchbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import component from Angular Material
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { Part1Component } from './main/part1/part1.component';
import { RegisterComponent } from './nav-link/register/register.component';
import { HomeComponent } from './home/home.component';
import { Part2Component } from './main/part2/part2.component';
import { Part3Component } from './main/part3/part3.component';
import { LoginComponent } from './nav-link/login/login.component';

@NgModule({
	declarations: [
		AppComponent,
		NavBarComponent,
		BackgroundSearchbarComponent,
		Part1Component,
		RegisterComponent,
		HomeComponent,
		Part2Component,
		Part3Component,
		LoginComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		HttpClientModule,
		MatToolbarModule,
		MatSidenavModule,
		MatListModule,
		MatButtonModule,
		MatIconModule,
		LayoutModule,
		ReactiveFormsModule,
		FormsModule,

		JwtModule.forRoot({

		config: {
        tokenGetter: function  tokenGetter() {
			return localStorage.getItem('access_token');},
				whitelistedDomains: ['localhost:5000'],
				blacklistedRoutes: ['http://localhost:5000/api/auth/login']
			}
    })
	],
	providers: [],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
