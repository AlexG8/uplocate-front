import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UsersService } from '../../shared/users.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: [ './register.component.css' ]
})
export class RegisterComponent implements OnInit {
	creationForm: FormGroup;

	constructor(private usersSvc: UsersService, private fb: FormBuilder, private router: Router) {}

	ngOnInit() {
		this.formUser();
	}

	formUser() {
		this.creationForm = this.fb.group({
			firstName: '',
			lastName: '',
			email: '',
			password: ''
		});
	}

	postUser() {
		if (this.creationForm.valid) {
			console.log(this.creationForm);
			this.usersSvc.createUser(this.creationForm.value).subscribe((data) => {
				console.log(data, 'Utilisateur Inscrit');
				this.router.navigate([ '/login' ]);
			});
		}
	}
}
