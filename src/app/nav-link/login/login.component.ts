import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UsersService } from '../../shared/users.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.css' ]
})
export class LoginComponent implements OnInit {
	creationForm: FormGroup;

	constructor(private usersSvc: UsersService, private fb: FormBuilder, private router: Router) {}

	ngOnInit() {
		this.formUserLogin();
	}

	formUserLogin() {
		this.creationForm = this.fb.group({
			email: '',
			password: ''
		});
	}

	userLogin() {
		if (this.creationForm.valid) {
			console.log(this.creationForm);
			this.usersSvc.loginUser(this.creationForm.value).subscribe((data) => {
				console.log(data, 'Utilisateur connecter');
			});
		}
	}
}
